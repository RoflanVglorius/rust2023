# Deadlines

| Group           | Task                                  | Score                    | Start            | Deadline         |
| ----------------|:-------------------------------------:|:------------------------:|:----------------:|:----------------:|
| Tutorial        | add                                   | 10                       | 21-02-2023 00:00 | 21-03-2023 23:59 |
| Intro           | combinations<br>conway<br>min-queue   | 100<br>100<br>100        | 21-02-2023 00:00 | 27-03-2023 23:59 |
| Std-collections | comm<br>prefix<br>lru-cache           | 100<br>200<br>300        | 28-02-2023 00:00 | 03-04-2023 23:59 |
| Traits          | trust<br>mpsc<br>flatmap<br>itertools | 100<br>200<br>200<br>300 | 11-03-2023 00:00 | 10-04-2023 23:59 |
| Modules         | trie                                  | 400 (bonus)              | 29-03-2023 00:00 | 15-04-2023 23:59 |
